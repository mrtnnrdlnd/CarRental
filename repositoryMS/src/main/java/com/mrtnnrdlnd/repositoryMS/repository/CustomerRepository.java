package com.mrtnnrdlnd.repositoryMS.repository;

import com.mrtnnrdlnd.repositoryMS.entities.Customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
