package com.mrtnnrdlnd.repositoryMS.repository;

import com.mrtnnrdlnd.repositoryMS.entities.Car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    
}
