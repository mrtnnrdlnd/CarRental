package com.mrtnnrdlnd.repositoryMS.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import com.mrtnnrdlnd.repositoryMS.entities.Car;
import com.mrtnnrdlnd.repositoryMS.model.CarModel;
import com.mrtnnrdlnd.repositoryMS.model.CarVO;
import com.mrtnnrdlnd.repositoryMS.repository.CarRepository;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("cars")
public class CarController {

    @Autowired
    CarRepository carRepository;

    Logger log;

    @PostConstruct
    private void init(){
        carRepository.save(new Car().setName("Volvo V70").setModel(CarModel.MEDIUM).setPricePerDay(105.0));
        carRepository.save(new Car().setName("Citroën C5").setModel(CarModel.MEDIUM).setPricePerDay(95.0));
        carRepository.save(new Car().setName("Audi A4").setModel(CarModel.MEDIUM).setPricePerDay(97.0));
        carRepository.save(new Car().setName("Toyota Corolla").setModel(CarModel.SMALL).setPricePerDay(87.0));
        carRepository.save(new Car().setName("Tesla Cybertruck").setModel(CarModel.LARGE).setPricePerDay(120.0));
        carRepository.save(new Car().setName("Tesla Cybertruck").setModel(CarModel.LARGE).setPricePerDay(125.0));
    }

    @Autowired
    CarController(Logger logger) {
        this.log = logger;
    }

    @GetMapping
    public List<CarVO> readAll() {
        List<CarVO> carVOs = new ArrayList<>();
        for (Car car : carRepository.findAll()) {
            carVOs.add(new CarVO(car));
        }
        return carVOs;
    }

    @PostMapping("create")
    public String create(@RequestBody CarVO carVO) {
        Car car = new Car();
        car.setModel(carVO.getModel())
            .setName(carVO.getName())
            .setPricePerDay(carVO.getPricePerDay());

        carRepository.save(car);

        return "Created: " + car;
    }

    @GetMapping("read/{id}")
    public CarVO read(@PathVariable("id") Long id) {
        CarVO carVO;
        try {
            Car car = carRepository.getById(id);
            carVO = new CarVO(car);
        } catch (EntityNotFoundException exception) {
            carVO = new CarVO().setId(-1);
            log.info(exception.getMessage());
        } 
        return carVO;
    }

    @PutMapping("update/{id}")
    public String update(@PathVariable("id") Long id, @RequestBody CarVO carVO) {
        String message = "";
        try {
            Car car = carRepository.getById(id);
            car
                .setPricePerDay(carVO.getPricePerDay())
                .setName(carVO.getName())
                .setModel(carVO.getModel());
            carRepository.save(car);
            message = "Updated: " + car;
        } catch(EntityNotFoundException exception) {
            message = "Could not update to " + carVO;
            log.info(exception.getMessage());
        }
        return message;
    }

    @DeleteMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        String message = "";

        try {
            Car car = carRepository.getById(id);
            carRepository.delete(car);
            message = "Deleted: " + car;
        } catch (EntityNotFoundException exception) {
            message = "Could not delete because entity with id: " + id + " did not exist.";
            log.info(exception.getMessage());
        }

        return message;
    }
  
}
