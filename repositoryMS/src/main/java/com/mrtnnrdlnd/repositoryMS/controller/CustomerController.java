package com.mrtnnrdlnd.repositoryMS.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import com.mrtnnrdlnd.repositoryMS.entities.Customer;
import com.mrtnnrdlnd.repositoryMS.model.CustomerVO;
import com.mrtnnrdlnd.repositoryMS.repository.CustomerRepository;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("customers")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    Logger log;

    @PostConstruct
    private void init(){
        customerRepository.save(new Customer().setName("Pippa Mediaslargas").setAddress("Villerkulla 5"));
        customerRepository.save(new Customer().setName("Findus Petsson").setAddress("Muckelbo street 2"));
        customerRepository.save(new Customer().setName("Bobba Fett").setAddress("Stjärnorna 42"));
        customerRepository.save(new Customer().setName("Thor Asgarv").setAddress("Hammarvägen 7"));
    }

    @Autowired
    CustomerController(Logger logger) {
        this.log = logger;
    }

    @GetMapping
    public List<CustomerVO> retrieveAll() {
        List<CustomerVO> customerVOs = new ArrayList<>();
        for (Customer customer : customerRepository.findAll()) {
            customerVOs.add(new CustomerVO(customer));
        }
        return customerVOs;
    }

    @PostMapping("create")
    public String create(@RequestBody CustomerVO customerVO) {
        Customer customer = new Customer()
            .setName(customerVO.getName())
            .setAddress(customerVO.getAddress());

        customerRepository.save(customer);

        return "Created: " + customer;
    }

    @GetMapping("read/{id}")
    public CustomerVO read(@PathVariable("id") Long id) {
        CustomerVO customerVO;
        try {
            Customer customer = customerRepository.getById(id);
            customerVO = new CustomerVO(customer);
        } catch (EntityNotFoundException exception) {
            customerVO = new CustomerVO().setId(-1);
            log.info(exception.getMessage());
        }
        return customerVO;
    }

    @PutMapping("update/{id}")
    public String update(@PathVariable("id") long id, @RequestBody CustomerVO customerVO) {
        String message = "";
        try {
            Customer customer = customerRepository.getById(id);
            customer
                .setAddress(customerVO.getAddress())
                .setName(customerVO.getName());
            customerRepository.save(customer);
            message = "Updated: " + customer;
        } catch(EntityNotFoundException exception) {
            message = "Could not update to " + customerVO;
            log.info(exception.getMessage());
        }
        return message;
    }

    @DeleteMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        String message = "";

        try {
            Customer customer = customerRepository.getById(id);
            customerRepository.delete(customer);
            message = "Deleted: " + customer;
        } catch (EntityNotFoundException exception) {
            message = "Could not delete because entity with id: " + id + " did not exist.";
            log.info(exception.getMessage());
        }

        return message;
    }
}
