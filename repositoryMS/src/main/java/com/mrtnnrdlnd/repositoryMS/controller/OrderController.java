package com.mrtnnrdlnd.repositoryMS.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import com.mrtnnrdlnd.repositoryMS.entities.Order;
import com.mrtnnrdlnd.repositoryMS.model.CarVO;
import com.mrtnnrdlnd.repositoryMS.model.CustomerVO;
import com.mrtnnrdlnd.repositoryMS.model.OrderVO;
import com.mrtnnrdlnd.repositoryMS.repository.CarRepository;
import com.mrtnnrdlnd.repositoryMS.repository.CustomerRepository;
import com.mrtnnrdlnd.repositoryMS.repository.OrderRepository;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("orders")
public class OrderController {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    CustomerRepository customerRepository;

    Logger log;

    @PostConstruct
    private void init(){
        orderRepository.save(new Order().setCarId(1).setCustomerId(1).setStartDate(LocalDate.now()).setStopDate(LocalDate.now().plusDays(10)));
        orderRepository.save(new Order().setCarId(2).setCustomerId(1).setStartDate(LocalDate.now().minusDays(10)).setStopDate(LocalDate.now().minusDays(1)));
        orderRepository.save(new Order().setCarId(3).setCustomerId(1).setStartDate(LocalDate.now().plusDays(1)).setStopDate(LocalDate.now().plusDays(2)));
        orderRepository.save(new Order().setCarId(4).setCustomerId(2).setStartDate(LocalDate.now().plusDays(2)).setStopDate(LocalDate.now().plusDays(5)));
        orderRepository.save(new Order().setCarId(5).setCustomerId(2).setStartDate(LocalDate.now()).setStopDate(LocalDate.now().plusDays(10)));
        orderRepository.save(new Order().setCarId(6).setCustomerId(3).setStartDate(LocalDate.now().minusDays(10)).setStopDate(LocalDate.now().minusDays(1)));
    }

    @Autowired
    OrderController(Logger logger) {
        this.log = logger;
    }

    @GetMapping
    public List<OrderVO> readAll() {
        List<OrderVO> orderVOs = new ArrayList<>();
        for (Order order : orderRepository.findAll()) {
            orderVOs.add(orderVOFrom(order));
        }
        return orderVOs;
    }

    @GetMapping("read/{id}")
    public OrderVO read(@PathVariable("id") Long id) {
        OrderVO orderVO;
        try {
            Order order = orderRepository.getById(id);
            orderVO = orderVOFrom(order);
        } catch (EntityNotFoundException exception) {
            orderVO = new OrderVO().setId(-1);
            log.info(exception.getMessage());
        }
        return orderVO;
    }

    @PostMapping("create")
    public String create(@RequestBody OrderVO orderVO) {
        String message = "";
        Order order = orderFrom(orderVO);
        try {
            orderVOFrom(order);
            orderRepository.save(order);
            message = "Created: " + order;
        } catch(EntityNotFoundException exception) {
            message = "Could not create specified order because either Car or Customer did not exist.";
            log.info(exception.getMessage());
        }
        return message;
    }

    @PutMapping("update/{id}")
    public String update(@PathVariable("id") Long id, @RequestBody OrderVO orderVO) {

        log.info(orderVO.toString());

        String message = "";
        try {
            Order order = orderRepository.getById(id);
            order.setCarId(orderVO.getCar().getId())
                .setCustomerId(orderVO.getCustomer().getId())
                .setStartDate(orderVO.getStartDate())
                .setStopDate(orderVO.getStopDate());
            orderRepository.save(order);
            message = "Updated: " + order;
        } catch(EntityNotFoundException exception) {
            message = "Could not update to " + orderVO;
            log.info(exception.getMessage());
        }
        return message;
    }

    @DeleteMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        String message = "";

        try {
            Order order = orderRepository.getById(id);
            orderRepository.delete(order);
            message = "Deleted: " + order;
        } catch (EntityNotFoundException exception) {
            message = "Could not delete because entity with id: " + id + " did not exist.";
            log.info(exception.getMessage());
        }

        return message;
    } 

    private OrderVO orderVOFrom(Order order) throws EntityNotFoundException {
        OrderVO orderVO = new OrderVO();
            orderVO.setId(order.getId())
                .setCar(new CarVO(carRepository.getById(order.getCarId())))
                .setCustomer(new CustomerVO(customerRepository.getById(order.getCustomerId())))
                .setStartDate(order.getStartDate())
                .setStopDate(order.getStopDate());
        return orderVO;
    }

    private Order orderFrom(OrderVO orderVO) {
        Order order = new Order();
        order.setCarId(orderVO.getCar().getId())
            .setCustomerId(orderVO.getCustomer().getId())
            .setStartDate(orderVO.getStartDate())
            .setStopDate(orderVO.getStopDate());
        return order;
    }

}
