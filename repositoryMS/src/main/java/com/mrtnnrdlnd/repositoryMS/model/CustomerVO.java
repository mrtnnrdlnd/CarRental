package com.mrtnnrdlnd.repositoryMS.model;

import com.mrtnnrdlnd.repositoryMS.entities.Customer;

public class CustomerVO {
    
    private long id;
    private String name;
    private String address;

    public CustomerVO() {
    }

    public CustomerVO(Customer customer) {
        this.id = customer.getId();
        this.name = customer.getName();
        this.address = customer.getAddress();
    }

    public long getId() {
        return id;
    }

    public CustomerVO setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CustomerVO setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public CustomerVO setAddress(String address) {
        this.address = address;
        return this;
    }
    

    

}
