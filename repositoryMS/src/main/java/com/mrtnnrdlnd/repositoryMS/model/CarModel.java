package com.mrtnnrdlnd.repositoryMS.model;

public enum CarModel {
    SMALL, MEDIUM, LARGE
}
