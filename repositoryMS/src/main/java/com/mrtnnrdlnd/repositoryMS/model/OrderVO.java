package com.mrtnnrdlnd.repositoryMS.model;


import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class OrderVO {

    private long id;
    private CustomerVO customer;
    private CarVO car;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate startDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate stopDate;

    public OrderVO() {
    }
    public long getId() {
        return id;
    }
    public OrderVO setId(long id) {
        this.id = id;
        return this;
    }
    public CustomerVO getCustomer() {
        return customer;
    }
    public OrderVO setCustomer(CustomerVO customer) {
        this.customer = customer;
        return this;
    }
    public CarVO getCar() {
        return car;
    }
    public OrderVO setCar(CarVO car) {
        this.car = car;
        return this;
    }
    public LocalDate getStartDate() {
        return startDate;
    }
    public OrderVO setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }
    public LocalDate getStopDate() {
        return stopDate;
    }
    public OrderVO setStopDate(LocalDate stopDate) {
        this.stopDate = stopDate;
        return this;
    }
    @Override
    public String toString() {
        return "OrderVO [car=" + car + ", customer=" + customer + ", id=" + id + ", startDate=" + startDate
                + ", stopDate=" + stopDate + "]";
    }

    
}
