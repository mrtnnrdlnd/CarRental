package com.mrtnnrdlnd.repositoryMS.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String address;

    public Customer() {
    }
    public long getId() {
        return id;
    }
    public Customer setId(long id) {
        this.id = id;
        return this;
    }
    public String getName() {
        return name;
    }
    public Customer setName(String name) {
        this.name = name;
        return this;
    }
    public String getAddress() {
        return address;
    }
    public Customer setAddress(String address) {
        this.address = address;
        return this;
    }
    @Override
    public String toString() {
        return "Customer [address=" + address + ", id=" + id + ", name=" + name + "]";
    } 
    
}
