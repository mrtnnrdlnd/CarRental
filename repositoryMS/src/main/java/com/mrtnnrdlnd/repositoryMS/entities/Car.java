package com.mrtnnrdlnd.repositoryMS.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mrtnnrdlnd.repositoryMS.model.CarModel;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double pricePerDay;
    private String name;
    private CarModel model;

    public Car() {
    }

    public long getId() {
        return id;
    }
    public Car setId(long id) {
        this.id = id;
        return this;
    }
    public double getPricePerDay() {
        return pricePerDay;
    }
    public Car setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
        return this;
    }
    public String getName() {
        return name;
    }
    public Car setName(String name) {
        this.name = name;
        return this;
    }
    public CarModel getModel() {
        return model;
    }
    public Car setModel(CarModel model) {
        this.model = model;
        return this;
    }

    @Override
    public String toString() {
        return "Car [id=" + id + ", model=" + model + ", name=" + name + ", pricePerDay=" + pricePerDay + "]";
    }  

}
