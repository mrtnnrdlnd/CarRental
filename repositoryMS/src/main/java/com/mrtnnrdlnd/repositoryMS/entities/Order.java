package com.mrtnnrdlnd.repositoryMS.entities;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long customerId;

    private long carId;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate startDate;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate stopDate;

    public Order() {
    }

    public long getId() {
        return id;
    }

    public Order setId(long id) {
        this.id = id;
        return this;
    }

    public long getCustomerId() {
        return customerId;
    }

    public Order setCustomerId(long customerId) {
        this.customerId = customerId;
        return this;
    }

    public long getCarId() {
        return carId;
    }

    public Order setCarId(long carId) {
        this.carId = carId;
        return this;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Order setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDate getStopDate() {
        return stopDate;
    }

    public Order setStopDate(LocalDate stopDate) {
        this.stopDate = stopDate;
        return this;
    }

    @Override
    public String toString() {
        return "Order [carId=" + carId + ", customerId=" + customerId + ", id=" + id + ", startDate=" + startDate
                + ", stopDate=" + stopDate + "]";
    }


    
  
}
