package com.mrtnnrdlnd.repositoryMS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepositoryMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepositoryMsApplication.class, args);
	}

}
