package com.mrtnnrdlnd.mainAPI.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrtnnrdlnd.mainAPI.model.CarModel;
import com.mrtnnrdlnd.mainAPI.model.CarVO;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

@SpringBootTest
public class CarServiceTest {

    private final static MockWebServer mockWebServer = new MockWebServer();
    @Autowired
    private CarService carService;
    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer.start();
        
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @BeforeEach
    void initialize() {
        String baseUrl = String.format("http://localhost:%s", 
        mockWebServer.getPort() + "/cars");
        carService.setBaseUrl(baseUrl);
    }

    @Test
    void testCreate() {
        
    }

    @Test
    void testDelete() {

    }

    @Test
    void testRead() throws JsonProcessingException {
        CarVO mockCar = new CarVO().setId(1).setName("Volvo V70").setModel(CarModel.MEDIUM).setPricePerDay(105.0);
        mockWebServer.enqueue(new MockResponse()
            .setBody(objectMapper.writeValueAsString(mockCar))
            .addHeader("Content-Type", "application/json"));

        CarVO recievedCarVO = carService.read(1l);

        Assertions.assertThat(recievedCarVO).isNotNull();
    }

    @Test
    void testReadAll() throws JsonProcessingException {

        List<CarVO> mockCarList = new ArrayList<>();
        mockCarList.add(new CarVO().setId(1).setName("Volvo V70").setModel(CarModel.MEDIUM).setPricePerDay(105.0));
        mockCarList.add(new CarVO().setName("Citroén C5").setModel(CarModel.MEDIUM).setPricePerDay(95.0));
        mockWebServer.enqueue(new MockResponse()
            .setBody(objectMapper.writeValueAsString(mockCarList))
            .addHeader("Content-Type", "application/json"));

        List<CarVO> recievedCarVOList = carService.readAll();

        Assertions.assertThat(recievedCarVOList).isNotNull();
        Assertions.assertThat(recievedCarVOList.size()).isEqualTo(2);

    }

    @Test
    void testUpdate() {

    }
}
