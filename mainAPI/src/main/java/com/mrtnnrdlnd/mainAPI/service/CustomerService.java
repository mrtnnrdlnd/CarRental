package com.mrtnnrdlnd.mainAPI.service;

import com.mrtnnrdlnd.mainAPI.model.CustomerVO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService extends AbstractService<CustomerVO> {

    @Autowired
    public CustomerService(Logger log) {
        super(CustomerVO.class, "customers");
        this.log = log;
    }
}
