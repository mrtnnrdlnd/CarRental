package com.mrtnnrdlnd.mainAPI.service;

import com.mrtnnrdlnd.mainAPI.model.CarVO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService extends AbstractService<CarVO> {

    @Autowired
    public CarService(Logger log) {
       super(CarVO.class, "cars");
       this.log = log;
    } 
}
