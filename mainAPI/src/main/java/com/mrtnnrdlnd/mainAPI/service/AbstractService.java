package com.mrtnnrdlnd.mainAPI.service;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

@Service
public abstract class AbstractService<T> {

    private String baseUrl = "http://localhost:8081/api/v1/";

    private WebClient webClient;
    private Class<T> VOType;
    protected Logger log;

    // public AbstractService(Class<T> VOType, String uri) {
    //     this.webClient = WebClient.builder()
    //             .baseUrl("http://localhost:8081/api/v1/")
    //             .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
    //             .build();
    //     this.VOType = VOType;
    //     this.uri = uri;
    // }

    public AbstractService(Class<T> VOType, String entity) {
        this.baseUrl += entity;
        this.webClient = WebClient.builder()
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
        this.VOType = VOType;
    }

    public List<T> readAll() {
        List<T> t = webClient.get()
                .uri(this.baseUrl)
                .retrieve()
                .bodyToFlux(VOType)
                .collectList()
                .block();
        return t;
    }

    public String create(T t) {
        String returnMessage = webClient.post()
            .uri(this.baseUrl + "/create")
            .body(Mono.just(t), VOType)
            .retrieve()
            .bodyToMono(String.class).block();

        log.info(t.toString());
        log.info(returnMessage);
        return returnMessage;
    }

    public T read(Long id) {
        T t = webClient.get()
                .uri(this.baseUrl + "/read/" + id)
                .retrieve()
                .bodyToMono(VOType)
                .block();

        return t;
    }

    public String update(Long id, T t) {
        String returnMessage = webClient.put()
            .uri(this.baseUrl + "/update/" + id)
            .body(Mono.just(t), VOType)
            .retrieve()
            .bodyToMono(String.class).block();

        log.info(t.toString());
        log.info(returnMessage);
        return returnMessage;
    }

    public String delete(Long id) {
        String returnMessage = webClient.delete()
        .uri(this.baseUrl + "/delete/" + id)
        .retrieve()
        .bodyToMono(String.class).block();

    log.info(returnMessage);
    return returnMessage;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
    
}
