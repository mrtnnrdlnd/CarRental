package com.mrtnnrdlnd.mainAPI.service;

import com.mrtnnrdlnd.mainAPI.model.OrderVO;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends AbstractService<OrderVO> {

    @Autowired
    public OrderService(Logger log) {
        super(OrderVO.class, "orders");
        this.log = log;
    }

    
}
