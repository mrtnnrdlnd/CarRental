package com.mrtnnrdlnd.mainAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

@Configuration
@EnableWebSecurity
public class BasicConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private CORSFilter myCorsFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder =
                PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(encoder.encode("password"))
                .roles("USER")
                .and()
                .withUser("admin")
                .password(encoder.encode("password"))
                .roles("USER", "ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //CORS
        http.addFilterBefore(myCorsFilter, ChannelProcessingFilter.class);

        http
                .authorizeRequests()
                .antMatchers("/cars").hasRole("USER")
                .antMatchers("/ordercar").hasRole("USER")
                .antMatchers("/updateorder").hasRole("USER")
                .antMatchers("/myorders").hasRole("USER")
                .antMatchers("/customers").hasRole("ADMIN")
                .antMatchers("/addcar").hasRole("ADMIN")
                .antMatchers("/deletecar").hasRole("ADMIN")
                .antMatchers("/updatecar").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
        
        http.csrf().disable();
    }
}
