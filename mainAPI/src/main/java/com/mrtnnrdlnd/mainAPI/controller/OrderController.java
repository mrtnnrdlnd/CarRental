package com.mrtnnrdlnd.mainAPI.controller;

import java.util.List;

import com.mrtnnrdlnd.mainAPI.model.OrderVO;
import com.mrtnnrdlnd.mainAPI.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://127.0.0.1:3000")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping("myorders")
    public List<OrderVO> retrieveAll() {
        return orderService.readAll();
    }

    @PostMapping("ordercar")
    public String create(@RequestBody OrderVO order) {
        return orderService.create(order);
    }

    @PutMapping("updateorder")
    public String update(@RequestBody OrderVO order) {
        return orderService.update(order.getId(), order);
    }
    
}
