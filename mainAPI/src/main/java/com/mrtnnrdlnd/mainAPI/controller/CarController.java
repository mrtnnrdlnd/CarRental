package com.mrtnnrdlnd.mainAPI.controller;

import java.util.List;

import com.mrtnnrdlnd.mainAPI.model.CarVO;
import com.mrtnnrdlnd.mainAPI.service.CarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin("http://127.0.0.1:3000")
public class CarController {
    
    @Autowired
    CarService carService;

    @GetMapping("cars")
    
    public List<CarVO> retrieveAll() {
        return carService.readAll();
    }

    @PostMapping("addcar")
    public String create(@RequestBody CarVO car) {
        return carService.create(car);
    }

    @DeleteMapping("deletecar")
    public String delete(@RequestBody CarVO car) {
        return carService.delete(car.getId());
    }

    @PutMapping("updatecar")
    public String update(@RequestBody CarVO car) {
        return carService.update(car.getId(), car);
    }


}
