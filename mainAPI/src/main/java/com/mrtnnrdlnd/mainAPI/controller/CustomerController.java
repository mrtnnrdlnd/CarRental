package com.mrtnnrdlnd.mainAPI.controller;

import java.util.List;

import com.mrtnnrdlnd.mainAPI.model.CustomerVO;
import com.mrtnnrdlnd.mainAPI.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://127.0.0.1:3000")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("customers")
    public List<CustomerVO> retrieveAll() {
        return customerService.readAll();
    }

    @PostMapping("addcustomer")
    public String create(@RequestBody CustomerVO customer) {
        return customerService.create(customer);
    }

    
}
