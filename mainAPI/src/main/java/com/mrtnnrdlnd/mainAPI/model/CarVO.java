package com.mrtnnrdlnd.mainAPI.model;


public class CarVO {

    private long id;
    private double pricePerDay;
    private String name;
    private CarModel model;

    public CarVO() {
    }

    public long getId() {
        return id;
    }
    
    public CarVO setId(long id) {
        this.id = id;
        return this;
    }
    
    public double getPricePerDay() {
        return pricePerDay;
    }
    
    public CarVO setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
        return this;
    }

    public String getName() {
        return name;
    }

    public CarVO setName(String name) {
        this.name = name;
        return this;
    }

    public CarModel getModel() {
        return model;
    }

    public CarVO setModel(CarModel model) {
        this.model = model;
        return this;
    }

    @Override
    public String toString() {
        return "CarVO [id=" + id + ", model=" + model + ", name=" + name + ", pricePerDay=" + pricePerDay + "]";
    }

    

    
    
}
