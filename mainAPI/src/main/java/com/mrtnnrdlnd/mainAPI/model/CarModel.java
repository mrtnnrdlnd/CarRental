package com.mrtnnrdlnd.mainAPI.model;

public enum CarModel {
    SMALL, MEDIUM, LARGE
}
